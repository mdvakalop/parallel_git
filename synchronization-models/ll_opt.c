//Implementation of optimistic synchronization model for simple linked lists

#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>
#include <pthread.h> /* for pthread_spinlock_t */

#include "../common/alloc.h"
#include "ll.h"

typedef struct ll_node {
	int key;
	struct ll_node *next;
	/* other fields here? */
	pthread_spinlock_t spinlock;

} ll_node_t;

struct linked_list {
	ll_node_t *head;
	/* other fields here? */
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;

	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;
	/* Other initializations here? */
	pthread_spin_init(&(ret->spinlock), 1);

	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;

	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;

	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

int ll_validate(ll_t *ll, ll_node_t *pred, ll_node_t *curr)
{
	ll_node_t *node = ll->head;

	while(node->key <= pred->key) {
		if (node == pred) 
			return pred->next == curr;
		node = node->next;
	}

	return 0;
}

int ll_contains(ll_t *ll, int key)
{
	ll_node_t *pred, *curr;
	int ret = 0;	

	while (1) {
		pred = ll->head;
		curr = pred->next;
		while(curr->key < key) {
			pred = curr;
			curr = curr->next;
		}
		if (curr->key == key) {
			pthread_spin_lock(&(pred->spinlock));
			pthread_spin_lock(&(curr->spinlock));
			
			if (ll_validate(ll, pred, curr)) {
				ret = 1;
				//ret = (curr->key == key); //needed?
				//printf("RET FROM CONTAINS: %d", ret);
				break;
			}
			//not validated, retry
			pthread_spin_unlock(&(pred->spinlock));
			pthread_spin_unlock(&(curr->spinlock));
		}
		else break; //key not found	 
	}

	if (ret) {
		pthread_spin_unlock(&(pred->spinlock));
		pthread_spin_unlock(&(curr->spinlock));
	}	

	return 0;
}

int ll_add(ll_t *ll, int key)
{
	ll_node_t *pred, *curr, *new_node;
	int ret = 0;

	while (1) {
		pred = ll->head;
		curr = pred->next;
		while(curr->key < key) {
			pred = curr;
			curr = curr->next;
		}
		if (curr->key > key) {
			pthread_spin_lock(&(pred->spinlock));
			pthread_spin_lock(&(curr->spinlock));

			if (ll_validate(ll, pred, curr)) {
				new_node = ll_node_new(key);
				new_node->next = curr;
				pred->next = new_node;
				ret = 1;
				break;
			}
			//not validated, retry
			pthread_spin_unlock(&(pred->spinlock));
			pthread_spin_unlock(&(curr->spinlock));
		}
		else break; //node with this key already exists
	}

	if (ret) {
		pthread_spin_unlock(&(pred->spinlock));
		pthread_spin_unlock(&(curr->spinlock));
	}

	return ret;
}

int ll_remove(ll_t *ll, int key)
{
	ll_node_t *pred, *curr;
	int ret = 0;

	while (1) {
		pred = ll->head;
		curr = pred->next;
		while(curr->key < key) {
			pred = curr;
			curr = curr->next;
		}
		if (curr->key == key) {
			pthread_spin_lock(&(pred->spinlock));
			pthread_spin_lock(&(curr->spinlock));
			
			if (ll_validate(ll, pred, curr) /*&& curr->key == key*/) {
				pred->next = curr->next;
				ret = 1;
				break;
			}
			//not validated, retry
			pthread_spin_unlock(&(pred->spinlock));
			pthread_spin_unlock(&(curr->spinlock));
		}
		else break; //if key not found (curr->key>key and curr->key!=key)
	}

	if (ret) {
		pthread_spin_unlock(&(pred->spinlock));
		pthread_spin_unlock(&(curr->spinlock));
	}

	return ret;
}

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
