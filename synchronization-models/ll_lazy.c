//Implementation of Laze dynchronization model for simple linked lists

#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>
#include <pthread.h> /* for pthread_spinlock_t */

#include "../common/alloc.h"
#include "ll.h"

typedef struct ll_node {
	int key;
	struct ll_node *next;
	/* other fields here? */
	pthread_spinlock_t spinlock;
	int marked; //if marked, not in list
} ll_node_t;

struct linked_list {
	ll_node_t *head;
	/* other fields here? */
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;

	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;
	/* Other initializations here? */
	pthread_spin_init(&(ret->spinlock), PTHREAD_PROCESS_SHARED);
	ret->marked = 0;

	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
	pthread_spin_destroy(&(ll_node->spinlock));
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;

	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;

	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

int ll_validate(ll_node_t *pred, ll_node_t *curr)
{
	return (!pred->marked && !curr->marked && pred->next == curr);
}

int ll_contains(ll_t *ll, int key)
{
	ll_node_t *curr = ll->head;
	
	while(curr->key < key)
		curr = curr->next;
	
	//fprintf(stderr, "Ret contains\n");
	
	return (curr->key == key && !curr->marked);
}

int ll_add(ll_t *ll, int key)
{
	ll_node_t *pred, *curr, *new_node;
	int ret = 0;

	while (1) {
		pred = ll->head;
		curr = pred->next;
		while(curr->key < key) {
			pred = curr;
			curr = curr->next;
		}
		if (curr->key > key) {
			pthread_spin_lock(&(pred->spinlock));
			pthread_spin_lock(&(curr->spinlock));
			
			if (ll_validate(pred, curr)) {
				//if (curr->key != key) {
					new_node = ll_node_new(key);
					new_node->next = curr;
					pred->next = new_node;
					ret = 1;
					break;
				//}
			}
			//not validated, retry
			pthread_spin_unlock(&(pred->spinlock));
			pthread_spin_unlock(&(curr->spinlock));
		}
		else break; //node with key already exists	 
	}

	if (ret) {
		pthread_spin_unlock(&(pred->spinlock));
		pthread_spin_unlock(&(curr->spinlock));
	}

	return 0;
}

int ll_remove(ll_t *ll, int key)
{
	ll_node_t *pred, *curr;
	int ret = 0;

	while (1) {
		pred = ll->head;
		curr = pred->next;
		while(curr->key < key) {
			pred = curr;
			curr = curr->next;
		}
		if (curr->key == key) {
			pthread_spin_lock(&(pred->spinlock));
			pthread_spin_lock(&(curr->spinlock));
			
			if (ll_validate(pred, curr)) {
				curr->marked = 1; //logical delete
				pred->next = curr->next; //physical delete
				ret = 1;
				break;
			}
			//not validated, retry
			pthread_spin_unlock(&(pred->spinlock));
			pthread_spin_unlock(&(curr->spinlock));
		}
		else break; //key not found	 
	}

	if (ret) {
		pthread_spin_unlock(&(pred->spinlock));
		pthread_spin_unlock(&(curr->spinlock));
	}

	return 0;
}

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
