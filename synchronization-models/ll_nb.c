//Implementation of nonblocking synchronization model for simple linked lists

#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>

#include "../common/alloc.h"
#include "ll.h"

static const long long mask = 1;

typedef struct ll_node {
	int key;
	struct ll_node *next;
	/* other fields here? */
} ll_node_t;

struct linked_list {
	ll_node_t *head;
	/* other fields here? */
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;
	long long val;

	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;
	/* Other initializations here? */
	val = (long long)ret & ~mask; //last bit: marked, initialize to 0
	ret = (ll_node_t*)val;

	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;

	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;

	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

ll_node_t *get_ll_node_ref(ll_node_t *node)
{
	return (ll_node_t*)((long long)node & ~mask);
}

int get_ll_node_mark(ll_node_t *node)
{
	return (int)((long long)node & mask);
}

int ll_find(ll_t *ll, int key, ll_node_t **ret_pred, ll_node_t **ret_curr)
{
	//printf("IN FIND\n");
	int mark = 0, retry = 0; 
	ll_node_t *pred, *curr;

	while (1) {
		retry = 0;
		pred = ll->head;
		curr = pred->next;
		//curr = get_ll_node_ref(curr);
		while (1) {
			mark = get_ll_node_mark(curr);
			//printf("MARKED: %d\n", mark);
			while (mark) {
				if(!(__sync_bool_compare_and_swap(&(pred->next), curr, curr->next))) {
					retry = 1;
					break; //if didn't get deleted, again from the beginning
				}
				//printf("deletion from find\n");
				pred = curr;
				curr = pred->next;
	 			mark = get_ll_node_mark(curr); //check if next one is marked
			}

			if (retry) 
				break;
			
			if (curr->key >= key) { //> for add, = for delete
				*ret_pred = pred;
				*ret_curr = curr;
				//printf("ff pred->next: %lld\n", (long long)pred->next);
                                //printf("ff curr: %lld\n", (long long)curr);
				//printf("back from find\n");
				return 1;
			}
			pred = curr;
			curr = curr->next;
			
		}
	}	

	return 0;
}

int ll_contains(ll_t *ll, int key)
{
	ll_node_t *curr = ll->head;
	
	//printf("IN contains\n");

	while(curr->key < key)
		curr = curr->next;

	return (curr->key == key && !get_ll_node_mark(curr));
}

int ll_add(ll_t *ll, int key)
{
	ll_node_t *pred, *curr, *new_node;

	//simple init for the pointers
	pred = ll->head;
	curr = pred;
	//pred = ll_node_new(-2);
	//curr = ll_node_new(-3); 
	//printf("IN add\n");
	while(1) {
		if (ll_find(ll, key, &pred, &curr)) {
			if (curr->key == key)
				return 0; //node with this key already exists
			else {
				new_node = ll_node_new(key);
				//printf("pred->next: %lld\n", (long long)pred->next);
				//printf("curr: %lld\n", (long long)curr);

				if(!__sync_bool_compare_and_swap(&(new_node->next), NULL, curr)) {
					fprintf(stderr, "ll_add: Something went wrong in the 1st c&s\n");
					return 0;
				}
				if(__sync_bool_compare_and_swap(&(pred->next), curr, new_node)) {
					//printf("back from add\n");
					return 1;
				}
				else {
					//printf("pred->next: %lld\n", (long long)pred->next);
					//printf("curr: %lld\n", (long long)curr);
					//printf("addr pred->next: %lld\n", (long long)get_ll_node_ref(pred->next));
					//printf("addr curr: %lld\n", (long long)get_ll_node_ref(curr));
					fprintf(stderr, "ll_add: Something went wrong in the 2nd c&s\n");
					//return 0;
				}
			}
		}
	}
	
	return 0;
}

int ll_remove(ll_t *ll, int key)
{
	ll_node_t *pred, *curr;

	//simple init for the pointers
        pred = ll->head;
        curr = pred;
	//printf("IN remove\n");
	while(1) {
		if (ll_find(ll, key, &pred, &curr)) {
			if (curr->key != key)
				return 0; //node with this key not found
			else {
				if(!__sync_bool_compare_and_swap(&curr, pred->next, (long long)curr|mask))
					continue; //retry
				
				//tries only once to physically remove the node
				__sync_bool_compare_and_swap(&(pred->next), curr, curr->next);
				//printf("back from remove\n");
				return 1;
			}
		}
	}
	
	return 0;
}

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
