int count_reqr=0, count_reqs=0; //requests for recv/send
    MPI_Request requestsr[4], requestss[4], reqr, reqs;

    //----Computational core----//   
    gettimeofday(&tts, NULL);
    #ifdef TEST_CONV
    for (t=0;t<T && !global_converged;t++) {
    #endif
    #ifndef TEST_CONV
    #undef T
    #define T 256
    for (t=0;t<T;t++) {
    #endif

        //*************TODO*******************//

        /*Compute and Communicate*/

        /*Add appropriate timers for computation*/

        //Swap
        swap = u_previous;
        u_previous = u_current;
        u_current = swap;

        count_reqr=0;
        count_reqs=0;


        //Communcation
        if (north >= 0) {
            //printf("PROC %d has north neighbor\n", rank);
            double *tmp_row;
            tmp_row = (double*)malloc(local[1]*sizeof(double));
            copy1d(&u_previous[1][1], tmp_row, local[1]);

            MPI_Isend(tmp_row, 1, local_row, north, t, MPI_COMM_WORLD, &reqs);
            requestss[count_reqs] = reqs; 
            count_reqs++;
            MPI_Irecv(&u_current[0][1], 1, local_row, north, t, MPI_COMM_WORLD, &reqr);
            requestsr[count_reqr] = reqr; //pairnei to req apo to Irecv epeidh einai teleytaio
            count_reqr++;
        }
        if (south >= 0) {
            //printf("PROC %d has south neighbor\n", rank);
            MPI_Irecv(&u_previous[local[0]+1][1], 1, local_row, south, t, MPI_COMM_WORLD, &reqr);
            requestsr[count_reqr] = reqr;
            count_reqr++;
        }
        if (east >= 0) {
            //printf("PROC %d has east neighbor\n", rank);
            MPI_Irecv(&u_previous[1][local[1]+1], 1, local_col, east, t, MPI_COMM_WORLD, &reqr);
            requestsr[count_reqr] = reqr;
            count_reqr++;
        }
        if (west >= 0) {
            //printf("PROC %d has west neighbor\n", rank);
            double *tmp_col;
            tmp_col = (double*)malloc(local[0]*sizeof(double)); 
            copy1d(&u_previous[1][1], tmp_col, local[0]);

            MPI_Isend(tmp_col, 1, local_col, west, t, MPI_COMM_WORLD, &reqs);
            requestss[count_reqs] = reqs; 
            count_reqs++;
            MPI_Irecv(&u_current[1][0], 1, local_col, west, t, MPI_COMM_WORLD, &reqr);
            requestsr[count_reqr] = reqr;
            count_reqr++;
        }


        int count;
        count = max(count_reqr, count_reqs);
        MPI_Status statuses[count];
        MPI_Waitall(count_reqr, requestsr, statuses); //barrier for recv requests, before computation
        //MPI_Waitall(count_reqs, requestss, statuses);

        //Computation
        gettimeofday(&tcs, NULL);
        GaussSeidel(u_previous, u_current, i_min, i_max, j_min, j_max, omega);
        gettimeofday(&tcf,NULL);
        tcomp+=(tcf.tv_sec-tcs.tv_sec)+(tcf.tv_usec-tcs.tv_usec)*0.000001;

        if (south >= 0) {
            //printf("PROC %d has south neighbor\n", rank);
            MPI_Isend(&u_current[local[0]][1], 1, local_row, south, t, MPI_COMM_WORLD, &reqs);
            requestss[count_reqs] = reqs; 
            count_reqs++;
        }
        if (east >= 0) {
            //printf("PROC %d has east neighbor\n", rank);
            MPI_Isend(&u_current[1][local[1]], 1, local_col, east, t, MPI_COMM_WORLD, &reqs);
            requestss[count_reqs] = reqs; 
            count_reqs++;
        }


        #ifdef TEST_CONV
        if (t%C==0) {
            //*************TODO**************//
            /*Test convergence*/
            gettimeofday(&tcons, NULL);
            converged = converge(u_previous, u_current, local[0], local[1]);
            MPI_Allreduce(&converged,&global_converged,1,MPI_INT,MPI_LAND,MPI_COMM_WORLD);
            //printf("GLOBAL: %d\n", global_converged);            
            gettimeofday(&tconf, NULL);
        }       
        #endif

        //************************************//

        MPI_Waitall(count_reqs, requestss, statuses); //barrier for send requests

    }
    gettimeofday(&ttf,NULL);