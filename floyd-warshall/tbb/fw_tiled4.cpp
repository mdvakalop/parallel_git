/*
  Faster
  Tiled version of the Floyd-Warshall algorithm.
  command-line arguments: N, B
  N = size of graph
  B = size of tile
  works only when N is a multiple of B
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"
#include "tbb/parallel_invoke.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/task.h"
#include "tbb/task_group.h"

inline int min(int a, int b);
inline void FW(int **A, int K, int I, int J, int N);
void graph_init_random(int **adjm, int seed, int n,  int m);

int main(int argc, char **argv)
{
     int **A;
     int i,j,k;
     struct timeval t1, t2;
     double time;

     if (argc != 4){
        fprintf(stdout, "Usage %s threads N B\n", argv[0]);
        exit(0);
     }

     const int B = atoi(argv[3]);
     const int N = atoi(argv[2]);
     const int nthreads = atoi(argv[1]);

     A=(int **)malloc(N*sizeof(int *));
     for(i=0; i<N; i++)A[i]=(int *)malloc(N*sizeof(int));
	i=0;
     graph_init_random(A,-1,N,128*N);

     gettimeofday(&t1,0);
	
     tbb::task_scheduler_init init(nthreads);
     tbb::task_group g;

     for(k=0;k<N;k+=B){
        FW(A,k,k,k,B);
	
    	for(int i=0; i<N; i+=B)
            if (i != k) {
                g.run( [=] {
    		        FW(A,k,i,k,B); } );
        		g.run( [=] {
        		    FW(A,k,k,i,B); } );
    	    }
    	g.wait();
    	      
    	for(int i=0; i<N; i+=B)
    	    if (i != k) {
    		    g.run( [=] {
    			    for(int j=0; j<k; j+=B)
    			        FW(A,k,i,j,B); } );
    		}

    	for(int i=0; i<N; i+=B)
    	    if (i != k) {
    		    g.run( [=] {
    			    for(int j=k+B; j<N; j+=B)
    			        FW(A,k,i,j,B); } );
    	}
    	
    	g.wait();
     }
     gettimeofday(&t2,0);

     time=(double)((t2.tv_sec-t1.tv_sec)*1000000+t2.tv_usec-t1.tv_usec)/1000000;
     printf("FW_TILED task_parallel, %d, %d, %.4f\n", N,B,time);

     return 0;
}

inline int min(int a, int b)
{
     if(a<=b)return a;
     else return b;
}

inline void FW(int **A, int K, int I, int J, int N)
{
     int i,j,k;
     
     for(k=K; k<K+N; k++)
        for(i=I; i<I+N; i++)
           for(j=J; j<J+N; j++)
              A[i][j]=min(A[i][j], A[i][k]+A[k][j]);
     

}

void graph_init_random(int **adjm, int seed, int n,  int m)
{
     unsigned  int i, j;

     srand48(seed);
     for(i=0; i<n; i++)
        for(j=0; j<n; j++)
           adjm[i][j] = abs((( int)lrand48()) % 1048576);

     for(i=0; i<n; i++)adjm[i][i]=0;
}
